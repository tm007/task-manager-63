package ru.tsc.apozdnov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.apozdnov.tm.component.Bootstrap;
import ru.tsc.apozdnov.tm.config.ContexConfig;

public final class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext cntx =
                new AnnotationConfigApplicationContext(ContexConfig.class);
        @NotNull final Bootstrap bootstrap = cntx.getBean(Bootstrap.class);
        bootstrap.run();
    }

}