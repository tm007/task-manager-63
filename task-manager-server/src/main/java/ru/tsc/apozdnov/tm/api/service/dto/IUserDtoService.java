package ru.tsc.apozdnov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;
import ru.tsc.apozdnov.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDtoModel> {

    @NotNull
    UserDtoModel create(@NotNull String login, @NotNull String password);

    @NotNull
    UserDtoModel create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    UserDtoModel create(@NotNull String login, @NotNull String password, @Nullable Role role);

    @Nullable
    UserDtoModel findByLogin(@NotNull String login);

    @Nullable
    UserDtoModel findByEmail(@NotNull String email);

    @NotNull
    void removeByLogin(@NotNull String login);

    @NotNull
    UserDtoModel setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    UserDtoModel updateUser(
            @NotNull String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}