package ru.tsc.apozdnov.tm.servlet;

import ru.tsc.apozdnov.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/task/create/*")
public class TaskCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        TaskRepository.getInstance().add("Task"+ UUID.randomUUID());
        resp.sendRedirect("/tasks");
    }

}