package ru.tsc.apozdnov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.*;

public class TaskRepository {

    @NotNull
    private final Map<String, Task> tasks = new HashMap<>();

    {
        add("Task-"+ UUID.randomUUID());
        add("Task-"+ UUID.randomUUID());
        add("Task-"+ UUID.randomUUID());
    }

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void add(@NotNull final String name) {
        final Task task = new Task(name);
        tasks.put(task.getId(), task);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    @NotNull
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @NotNull
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

}