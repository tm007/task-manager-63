<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/header.jsp"/>

<h1>Редактирование проекта</h1>

<form action="/project/edit?id=${project.id}" method="post">

    <input type="hidden" name="id" value="${project.id}"/>

    <p>
    <div>Название:</div>
    <div>
        <input type="text" name="name" value="${project.name}"/>
    </div>
    </p>

    <p>
    <div>Описание:</div>
    <div>
        <input type="text" name="description" value="${project.description}"/>
    </div>
    </p>

    <p>
    <div>Статус:</div>
    <select name="status">
        <c:forEach var="status" items="${statuses}">
            <option
                    <c:if test="${project.status == status}">selected="selected"</c:if> value="${status}">
                    ${status.displayName}
            </option>
        </c:forEach>
    </select>
    </p>

    <p>
    <div>Дата начала:</div>
    <div>
        <input type="date" name="dateBegin"
               value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateBegin}" />"/>
    </div>
    </p>

    <p>
    <div>Дата окончания:</div>
    <div>
        <input type="date" name="dateEnd" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateEnd}" />"/>
    </div>
    </p>

    <button type="submit">Сохранить</button>
</form>

<jsp:include page="../include/footer.jsp"/>